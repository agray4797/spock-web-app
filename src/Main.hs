{-# LANGUAGE OverloadedStrings #-}
module Main where

import Web.Spock
import Web.Spock.Config
import Web.Spock.Lucid (lucid)
import Lucid
import Data.Text (Text)
import Data.IORef
import Control.Monad (forM_)
import Control.Monad.IO.Class (liftIO)
import Data.Semigroup ((<>))

data Note = Note {author :: Text, content :: Text}
newtype ServerState = ServerState {notes :: IORef [Note]}
type Server a = SpockM () () ServerState a

app :: Server ()
app = do
  get root $ do
    notes' <- getState >>= (liftIO . readIORef . notes)
    lucid $ do
      head_ $ do
        title_ "Spock NoteKeeper"
      body_ [style_ "background-color: lightblue; font-family: Calibri;"] $ do
        h1_ "NoteKeeper - Notes"
        p_ "See notes below:"
        ul_ $ forM_ notes' $ \note -> li_ $ do
          toHtml (author note)
          ": "
          toHtml (content note)
        h2_ "New Note"
        form_ [method_ "post"] $ do
          label_ $ do
            "Author: "
            input_ [name_ "author"]
          label_ $ do
            "Contents: " 
            textarea_ [name_ "content"] ""
          input_ [type_ "submit", value_ "Add"]
  post root $ do
    author <- param' "author"
    content <- param' "content"
    notesRef <- notes <$> getState
    liftIO $ atomicModifyIORef' notesRef $ \notes -> (notes <> [Note author content], ())
    redirect "/"

main :: IO ()
main = do
    st <- ServerState <$> newIORef [Note "Adam" "Sample Note A", Note "Adam" "Sample Note 2"]
    cfg <- defaultSpockCfg () PCNoDatabase st
    runSpock 8080 (spock cfg app)
